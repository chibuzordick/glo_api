# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 21:27:13 2017

@author: Dickson Chibuzo
"""
import json
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from passlib.apps import custom_app_context as pwd_context
from flask_httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()


from mongoengine import *
import datetime

connect('glotraffic_db', host='localhost', port=27017)

#creating the model for the users alongside the schema (making them subclasses of the Document class)
#creating a model for the subscription alonside the schema
class Subscription (Document):
    name= StringField(required=True, unique=True)
    sub_code=StringField(required=True, unique=True)
    duration=StringField(required=True)
    cost=IntField(required = True)
    note=StringField()
#adminUser table    
class AdminUsers (Document):
    user_name = StringField(required=True)
    password = StringField()
    ID = IntField(min_value=1)
    created_at= DateTimeField(default=datetime.datetime.now)
    
    def hash_password(self, password):
        p= pwd_context.encrypt(password)
        return p

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)
    
    def create_a_user(self, username, password):
      AdminUsers(user_name=username, password=password, ID=AdminUsers.objects.count() + 1).save()

    
    def generate_auth_token(self, expiration = 600):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.ID })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = AdminUsers.query.get(data['ID'])
        return user
        
        
class Users (Document):
    Phone_number = StringField(required=True, max_length=11, unique=True)
    sub_plan = ReferenceField(Subscription)
    home_address= StringField(required=True)
    home_address2= StringField(required=False)
    note= StringField(required=False)
    subscribed=BooleanField(required=True, default=False)
    date_sub= DateTimeField()
    date_sub_expires= DateTimeField()
    created_at= DateTimeField(default=datetime.datetime.now)
    
    
    #do a function that will make the user to 
    
'''post_1 = Users(
    Phone_number='0803316369',
    home_address='Some engaging content',
    #author='Scott'
)
post_1.save()       # This will perform an insert
print(post_1.Phone_number)
post_1.Phone_number = '08033163697'
post_1.save()       # This will perform an atomic edit on "title"
print(post_1.Phone_number)'''




'''
for test I will initialize the Subscription table with some values

'''
    
'''sub_1 = Subscription(
name='Weekly Plan',
sub_code='WP',
cost= 50,
note="The first test plan",
    #author='Scott'
)
sub_2 = Subscription(
name='Daily Plan',
sub_code='DP',
cost= 100,
note="The second test plan",
    #author='Scott'
)
sub_1.save()       # This will perform an insert
sub_2.save()'''

for sub in Subscription.objects:
    print sub.name, sub.cost
    
    
#creating the table to hold the report after every request
class Report_table(Document):
    who_makes_request = ReferenceField(Users)
    message= StringField(required=True)
    result = StringField(required=True)
    
    
from flask import Flask, abort, request, g, jsonify, Response, url_for, session

app =Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'

@app.route('/home')#here i will return the template they will use to send message and also that will have the list of all the links
def index ():
    return "my first web app in python using flask"
    
    

 #the function to create the user   
 #the number making the request and the message
 #message will be like: WP, plot 4 elias drive bako estate, suite 20 yaba
@app.route('/api/subscribe', methods=['GET'])#here they will subscribe for the service
def sub():
    if request.method=='GET' :
        num= request.args.get('number')
        mess= request.args.get('message')
        format_mess = mess.split(',')
        d_plan=""
        home_ad1= format_mess[1]
        home_ad2=""
        duration= ""
        for sub in Subscription.objects:
            if sub.sub_code == format_mess[0]:
                d_plan= sub
                duration = sub.duration
        if d_plan == "" :
            return (jsonify({'status': 'failed', 'message':num+' You entered a wrong code'}))
        if len(format_mess)>2:
            home_ad2=format_mess[2]
            #return "got here2" +home_ad2
        user = Users.objects(Phone_number=num).first()
        if user is not None :
            if user.subscribed == False:
                user.sub_plan=d_plan
                user.subscribed=True
                user.date_sub=datetime.datetime.now
                user.date_sub_expires=datetime.datetime.now + datetime.timedelta(days=duration)
                return (jsonify({'status': 'ok', 'message':num+' you have successfully subscribed for this'+d_plan.name}))
            else:
                return (jsonify({'status': 'failed', 'message':num+' You have already subscribed for this plan before'}))
        if len(format_mess) > 1 and len(format_mess) <= 3 and d_plan!="" :
            
            post_1 = Users(
                                Phone_number= num,
                                sub_plan=d_plan,
                                subscribed=True,
                                home_address=home_ad1,
                                home_address2 = home_ad2,
                                date_sub=datetime.datetime.now,
                                date_sub_expires=datetime.datetime.now + datetime.timedelta(days=duration)
                                
                            )
            try:
                post_1.save()
                return (jsonify({'status': 'ok', 'message':num+' you have successfully subscribed for this'+d_plan.name}))
            except:
                return (jsonify({'status': 'failed', 'message':num+' You have already subscribed for this plan before'}))
                 
                
        else:
            return (jsonify({'status': 'failed', 'message':num+' You have typed in a wrong code and this cant be processed'}))

            
#the function to add new subscription plan by user
@app.route('/api/add/subscription')
def add_sub():
    names = request.args.get('names')
    code = request.args.get('code')
    costs = request.args.get('cost')
    notes = request.args.get('note')
    try:
        sub_1 = Subscription(
            name=names,
            sub_code=code,
            cost= costs,
            note=notes,
            
            )
        sub_1.save()
        return (jsonify({'status': 'ok', 'message':' new code added successfully'}))
    except:
        return (jsonify({'status': 'failed', 'message':' this code has been added before'}))
        
            
#the function to edit        
@app.route('/api/edit/user', methods=['POST','GET'])
def edit_user():
    number = request.args.get('number')
    message = request.args.get('message')
    if request.method =='GET':
        #take the new details of the user I want to edit
        #they will send a message to edit 
        for user in Users.objects:
            if number == user.Phone_number and user.subscribed==False:
                mess=message
                format_mess = mess.split(',')
                home_ad1= ""
                home_ad2=""
                if len(format_mess)>=2:
                    if len(format_mess)>2:
                        home_ad2=format_mess[2]
                    if len(format_mess)>1:
                        home_ad1= format_mess[1]
                        
                    
                if len(format_mess) > 1 and len(format_mess) <= 3  and format_mess[0] == "EDIT":
                    if (home_ad1 != ""):
                        user.home_address= home_ad1
                    if home_ad2 != "" :
                        user.home_address2=home_ad2
                    try:
                        user.save()
                    except:
                        return (jsonify({'status': 'failed', 'message':' no changes found'}))
                    return  (jsonify({'status': 'ok', 'message':number+' you have successfully edited your details'}))
                else :
                    return (jsonify({'status': 'failed', 'message':number+' type in correct code or you dont have a current plan'}))
                
        
        

@app.route('/api/unsub/user', methods=['POST','GET'])
#the funtion to delete a customer and the function to return the home or unsubscribe
def unsub_user():
    number = request.args.get('number')
    message = request.args.get('message')
    format_mess = message.split(',')
    for user in Users.objects:
        if number == user.Phone_number and len(format_mess)==1 and format_mess[0]=="UNSUB" :
            user.subscribed=False
            user.save()
            return (jsonify({'status': 'ok', 'message':' user deleted successfully'}))
            
    return (jsonify({'status': 'failed', 'message':' An error occured'}))
#function to list and search  
@app.route('/api/user', methods=['GET'])
def list_users():
    list_all_users=list()
    t = ''
    if session.get('api_session_token')is not None:
        t = session['api_session_token']
    t_recieved= request.args.get('token')
    if request.method=='GET' and t==t_recieved:
        for user in Users.objects:
            
            per_user=dict()
            if user.subscribed == False:
                per_user= {'number':user.Phone_number, 
                       'subscription':"Not subscribed",
                       }
            else:
                per_user= {'number':user.Phone_number, 
                       'subscription':user.sub_plan.name,
                       'code':user.sub_plan.sub_code
                       }
            list_all_users.append(per_user)
        return Response(json.dumps({'numbers':list_all_users}))
    else:
        return Response(json.dumps({'error':"failed"}))

        
@app.route('/api/billing', methods=['GET'])
def list_billings():
    list_all_users=list()
    t = ''
    if session.get('api_session_token')is not None:
        t = session['api_session_token']
    t_recieved= request.args.get('token')
    if request.method=='GET' and t==t_recieved:
        for user in Users.objects(subscribed=True):
            
            per_user=dict()
            
            per_user= {'number':user.Phone_number, 
                       'subscribe':user.date_sub,
                       'due':user.dat_sub_expires
                       }
            list_all_users.append(per_user)
        return Response(json.dumps({'billings':list_all_users}))
    else:
        return Response(json.dumps({'error':"failed"}))
        
        
@app.route('/api/money', methods=['GET'])  
def money():
    list_all_users=list()
    t = ''
    if session.get('api_session_token')is not None:
        t = session['api_session_token']
    t_recieved= request.args.get('token')
    if request.method=='GET' and t==t_recieved:
        for user in Users.objects:
            
            per_user=dict()
            if user.subscribed == True:
                per_user= {'number':user.Phone_number, 
                       'sub_type':user.sub_plan.name,
                       'Amount':user.sub_plan.cost
                       }
                
            list_all_users.append(per_user)
        return Response(json.dumps({'moneys':list_all_users}))
    else:
        return Response(json.dumps({'error':"failed"}))



    
@app.route('/api/search')
def search_user():
    number = request.args.get('number')
    found = ""
    if request.method=='GET':
        for user in Users.objects:
            if user.Phone_number == number:
                found = user
                details= dict()
                details = {'Phone_number':found.Phone_number}
                return Response(json.dumps({'status':'ok', 'message':details}))
            else:
                return Response(json.dumps({'status':'failed', 'message':'What you are looking for is not here'}))
                
@app.route('/api/request/traffic', methods=['GET'])
def request_log():
    by_who = request.form.get('phone_number')
    mess = request.form.get('message')
    res = request.form.get('result')
    if request.method=='GET' :
        for user in Users.objects:
            if(user.Phone_number == by_who and user.subscribed==True):
                requester = Report_table(who_makes_request = user, message = mess, result= res)
                requester.save()
                return (jsonify({'status': 'ok', 'message':'The request for update saved'}))
                
            else:
                return (jsonify({'status': 'Failed', 'message':'Your have not subscribed for this service'}))
 
            
@app.route('/delete', methods=['POST','GET'])
#the funtion to delete a customer and the function to return the home or unsubscribe
def delete_report():
    for user in Report_table.objects:
        user.delete()
    return (jsonify({'status': 'ok', 'message':' user deleted successfully'}))
            
    return (jsonify({'status': 'failed', 'message':' An error occured'}))
    

@app.route('/api/results', methods=['GET'])
def list_reports():
    t = ''
    if session.get('api_session_token')is not None:
        t = session['api_session_token']
    t_recieved= request.args.get('token')
    list_all_reports=list()
    if request.method=='GET' and t == t_recieved:
        for user_request in Report_table.objects:
            per_report=dict()
            per_report= {'number':user_request.who_makes_request.Phone_number,
                         'message':user_request.message,
                         'price':user_request.who_makes_request.sub_plan.cost,
                         'result':user_request.result
                       }
            list_all_reports.append(per_report)
        return Response(json.dumps({ 'results':list_all_reports}))
    else:
        return Response(json.dumps({ 'error':'failed'}))
        
@app.route('/api/query', methods=['GET'])
def list_person_rep():
    t = ''
    if session.get('api_session_token')is not None:
        t = session['api_session_token']
    t_recieved= request.args.get('token')
    phone = request.form.get('phone_number')
    list_all_reports=list()
    if request.method=='GET' and t == t_recieved:
        for user_request in Report_table.objects(Phone_number=phone):
            if user_request.who_makes_request.Phone_number == phone:
                per_report=dict()
                per_report= {'Phone_number':user_request.who_makes_request.Phone_number,
                         'message':user_request.message,
                         'price':user_request.who_makes_request.sub_plan.cost,
                         'result':user_request.result
                       }
                list_all_reports.append(per_report)
        return Response(json.dumps({ 'results':list_all_reports}))
    else:
        return Response(json.dumps({ 'error':'failed'}))

        

#register new adminuser
@app.route('/api/register/users', methods = ['POST'])
def new_user():
    username = request.form.get('email')
    password = request.form.get('password')
    #username = uname
    #password = passwo
    if username is None or password is None:
        abort(400) # missing arguments
    if AdminUsers.objects(user_name = username).first() is not None:
        abort(400) # existing user
    post_1 = AdminUsers()
    passw=post_1.hash_password(password)
    #post_1.create_a_user(username,passw)
    
    try:
        post_1.create_a_user(username,passw)
        
        return (jsonify({'status':'ok', 'message':'New Admin User added'}))
        
    except:
        return Response(json.dumps({'status':'failed', 'message':'An error occured'}))

@app.route('/get/password/<string:username>')
def get_pass(username):
    u = AdminUsers.objects(user_name=username).first()
    if u is not None:
        return (jsonify({'status':u.password}))


    
@app.route('/api/users/<int:id>')
def get_user(id):
    user = AdminUsers.objects(ID=id).first()
    if not user:
        abort(400)
    return jsonify({'Username': user.user_name, 'token':user.generate_auth_token()})
#to verify the password  

@app.route('/api/token')
def get_auth_token():
    if g.current_user:
        token = g.current_user.generate_auth_token(600)#600seconds or 10 minutes
        return jsonify({'token': token.decode('ascii'), 'duration': 600})
    
    
    
    
@app.route('/api/login', methods=['POST'])
def login():
    status=""
    token=None
    email = request.json.get('email')
    password = request.json.get('password')
    user = AdminUsers.objects(user_name=email).first()
    if user and user.verify_password(password):
        session['logged_in'] = True
        session['api_session_token'] = token
        status = "Ok"
        token =user.generate_auth_token(600)
        session['api_session_token'] = token
    else:
        status = "Failed"
        token =None
    return jsonify({'message': status, "token":token})
        

@app.route('/api/logout')
def logout():
    session.pop('logged_in', None)
    return jsonify({'status': 'success'})

    
@auth.verify_password
def verify_password(username_or_token, password):
     # first try to authenticate by token
    user = AdminUsers.verify_auth_token(username_or_token)
    #return "found user"
    if not user:
        # try to authenticate with username/password
        user = AdminUsers.objects(user_name = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    session['logged_in'] = True
    g.current_user = user
    return True
    
@app.route('/api/home', methods=['GET'])
def home():
    t = session['api_session_token']
    t_recieved= request.args.get('token')
    if t == t_recieved:
        users_count= Users.objects.count()
        sales_count= Users.objects(subscribed=True)
        counter = 0 
        for s in sales_count:
            counter = counter+1
        return jsonify({'user_total': users_count, "sale_total":counter})
    else:
        return jsonify({'error': "failed"})
    

    
if __name__ == "__main__":
    app.run(debug=True)
    

    
    
